Please read below link details before initializing the main.py code

1: install python with this link : https://www.python.org/downloads/
2: create a workfolder which you will store csv file
3: Goto working work folder with cd command and excecute this command to install virtual environemtn :
	pip install virtualenv
		if error occures use (pip install virtualenv --user)
4: We need to install firebase admin, google cloud fire store. use this command :
	pip install firebase-admin google-cloud-firestore
5: Take the service key from firestore -> project setting -> service accounts -> select python and download the .json key 
    and paste the file in work directory.
6: make sure .csv file, main.py, service key(.json) file in the same work folder.
7. execute the python code and referesh firestore.
All the data in csv file will be in firestore.


https://medium.com/@cbrannen/importing-data-into-firestore-using-python-dce2d6d3cd51